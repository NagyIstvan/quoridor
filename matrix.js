var buttonRight;
var buttonLeft;
var buttonUp;
var buttonDown;
var buttonUpLeft;
var buttonUpRight;
var buttonDownLeft;
var buttonDownRight;
var oldClickX;
var oldClickY;
var clickX;
var clickY;
var sp;
var fenceX, fenceY, putted = 0;
const noFence = 1;
const noPawn = 0;
const posPawn1 = 2;
const posPawn2 = 3;
const roundPlayer1 = 1;
const roundPlayer2 = 2;
const fence = 4;
const margin = 5;
var roundP = roundPlayer1;
var puttedFencePawn1 = 0;
var puttedFencePawn2 = 0;
var col1 = 'grey';
var col2 = 'grey';
var win1 = false;
var win2 = false;
var cui;
var cuj;
var pli;
var plj;
var lastX=8;
var lastY=16;
var wayPawn1 = [];
var wayPawn2 = [];
var st = false;
var first;


class Tabla {

  init() {
    {
      this.tabla = [];
      for (var i = 0; i < 20; i++) {
        this.tabla[i] = [];
        for (var j = 0; j < 20; j++) {
          if (i == 17 || j == 17 || i == 18 || j == 18 || i == 20 || j == 20) {
            let valueObj = {
              value: margin
            }
            this.tabla[i][j] = valueObj;
          } else if (i % 2 == 0 && j % 2 == 0) {
            let valueObj = {
              value: noPawn
            }
            this.tabla[i][j] = valueObj;
          } else {
            let valueObj = {
              value: noFence
            }
            this.tabla[i][j] = valueObj;
          }
        }
      }
      first = floor(random(2));
      if (first) roundP = roundPlayer1;
      else roundP = roundPlayer2;
      this.tabla[0][8].value = posPawn1;
      this.tabla[16][8].value = posPawn2;
      win1 = false;
      win2 = false;
      puttedFencePawn1 = 0;
      puttedFencePawn2 = 0;

    }
  }
  clicked() {
    hideArrows();
    let hide = 0;
    if (!win1 && !win2)
      for (var i = 0; i < 17; i++)
        for (var j = 0; j < 17; j++) {
          if (i % 2 == 0 && j % 2 == 0) {
            if (this.tabla[i][j].value == posPawn1 && roundP == roundPlayer1 && putted != 1) {
              let x1 = floor(getRectSize() + getTableFromCanvas() + sp * j)
              let x2 = floor(2 * getRectSize() + getTableFromCanvas() + sp * j)
              let y1 = floor(getRectSize() + getTableFromCanvas() + sp * i)
              let y2 = floor(2 * getRectSize() + getTableFromCanvas() + sp * i)
              if (mouseX >= x1 && mouseX <= x2 && mouseY >= y1 && mouseY <= y2) {
                setSelect(true);
                tabla.drawArrows(posPawn1, roundPlayer2, posPawn2);
              } else hide++;
            }
            if (this.tabla[i][j].value == posPawn2 && roundP == roundPlayer2 && !CPU && putted != 1) {
              let x1 = floor(getRectSize() + getTableFromCanvas() + sp * j)
              let x2 = floor(2 * getRectSize() + getTableFromCanvas() + sp * j)
              let y1 = floor(getRectSize() + getTableFromCanvas() + sp * i)
              let y2 = floor(2 * getRectSize() + getTableFromCanvas() + sp * i)
              if (mouseX >= x1 && mouseX <= x2 && mouseY >= y1 && mouseY <= y2) {
                setSelect(true);
                tabla.drawArrows(posPawn2, roundPlayer1, posPawn1);
              } else hide++;
            }
            if (hide == 2) setSelect(false);
          }
        }
  }
  drawPawns() {
    var i, j;
    sp = (getRectSize() / 2 + getSpaceBetweenRects() / 2);
    for (var x = 0; x < 17; x++)
      for (var y = 0; y < 17; y++) {
        if (this.tabla[x][y].value == posPawn1) {
          i = x;
          j = y - 8;
          fill('yellow');
          strokeWeight(3);
          stroke("#292726");
          triangle(getTable() / 2 + getCirckleSize() + j * sp, (getRectSize() + getTableFromCanvas() + getRectSize() / 1.2) + i * sp, getTable() / 2 + j * sp, (getRectSize() + getTableFromCanvas() + getCirckleSize() / 2) + i * sp, getTable() / 2 - getCirckleSize() + j * sp, (getRectSize() + getTableFromCanvas() + getRectSize() / 1.2) + i * sp);
          circle(getTable() / 2 + j * (sp), getRectSize() + 2 * getTableFromCanvas() + getCirckleSize() / 2 + i * (sp), getCirckleSize());
        }
        if (this.tabla[x][y].value == posPawn2) {
          i = y - 8;
          j = x - 16;
          fill('red');
          strokeWeight(3);
          stroke("#292726");
          triangle(getTable() / 2 + getCirckleSize() + i * sp, getTable() - (getRectSize() + getTableFromCanvas() + getRectSize() / 1.2) + j * sp, getTable() / 2 + i * sp, getTable() - (getRectSize() + getTableFromCanvas() + getCirckleSize() / 2) + j * sp, getTable() / 2 - getCirckleSize() + i * sp, getTable() - (getRectSize() + getTableFromCanvas() + getRectSize() / 1.2) + j * sp);
          circle(getTable() / 2 + i * sp, getTable() - (getRectSize() + 2 * getTableFromCanvas() + getCirckleSize() / 2) + j * sp, getCirckleSize());
        }
        strokeWeight(1);
        stroke("black");
        if (this.tabla[x][y].value == fence)
          if (x % 2 == 0) {
            fill('grey');
            strokeWeight(2);
            stroke("#292726");
            rect(2 * getRectSize() + getTableFromCanvas() + sp * (y - 1), 1.6 * getRectSize() + getTableFromCanvas() + sp * (x - 1), getSpaceBetweenRects(), getRectSize());
            strokeWeight(1);
            stroke("black");
          }
        else if (y % 2 == 0) {
          fill('grey');
          strokeWeight(2);
          stroke("#292726");
          rect(1.6 * getRectSize() + getTableFromCanvas() + sp * (y - 1), 2 * getRectSize() + 2 * getTableFromCanvas() - getSpaceBetweenRects() / 3 + sp * (x - 1), getRectSize(), getSpaceBetweenRects());
          strokeWeight(1);
          stroke("black");
        } else {
          fill('grey');
          strokeWeight(2);
          stroke("#292726");
          rect(2 * getRectSize() + getTableFromCanvas() + sp * (y - 1), 2 * getRectSize() + 2 * getTableFromCanvas() - getSpaceBetweenRects() / 3 + sp * (x - 1), getSpaceBetweenRects(), getSpaceBetweenRects());
          strokeWeight(1);
          stroke("black");
        }

        if (this.tabla[16][y].value == posPawn1) win1 = true;
        if (this.tabla[0][y].value == posPawn2) win2 = true;
      }

  }
  redrawPawn(y, x, pawn) {
    this.tabla[x][y].value = pawn;
    stepSound();

  }
  drawArrows(pos, r, pos2) {
    for (var x = 0; x < 17; x++)
      for (var y = 0; y < 17; y++) {
        if (this.tabla[x][y].value == pos) {
          var i = y;
          var j = x;
        }
      }
    if (j > 0 && this.tabla[j - 1][i].value != fence) {
      buttonUp.show();
      buttonUp.position(getRectSize() + 1.25 * getSpaceBetweenRects() + sp * i, getSpaceBetweenRects() / 4 + sp * j);
      buttonUp.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      if (this.tabla[j - 2][i].value == pos2 && this.tabla[max(0, j - 3)][i].value != fence && j - 4 >= 0)
        buttonUp.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i, j - 4, pos);
          roundP = r;
          hideArrows();
        });
      else if (this.tabla[j - 2][i].value == noPawn)
        buttonUp.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i, j - 2, pos);
          roundP = r;
          hideArrows();
        });
      else buttonUp.hide();
    }

    if (i < 16 && this.tabla[j][i + 1].value != fence) {
      buttonRight.show();
      buttonRight.position(2 * (getRectSize() + 1.1 * getSpaceBetweenRects()) + sp * i, getRectSize() + 1.25 * getSpaceBetweenRects() + sp * j);
      buttonRight.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      if (this.tabla[j][i + 2].value == pos2 && this.tabla[j][i + 3].value != fence && i + 4 <= 16)
        buttonRight.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i + 4, j, pos);
          roundP = r;
          hideArrows();
        });
      else if (this.tabla[j][i + 2].value == noPawn)
        buttonRight.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i + 2, j, pos);
          roundP = r;
          hideArrows();
        });
      else buttonRight.hide();
    }

    if (j < 16 && this.tabla[j + 1][i].value != fence) {
      buttonDown.show();
      buttonDown.position(getRectSize() + 1.25 * getSpaceBetweenRects() + sp * i, 2 * (getRectSize() + 1.1 * getSpaceBetweenRects()) + sp * j);
      buttonDown.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      if (this.tabla[j + 2][i].value == pos2 && this.tabla[j + 3][i].value != fence && j + 4 <= 16)
        buttonDown.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i, j + 4, pos);
          roundP = r;
          hideArrows();
        });
      else if (this.tabla[j + 2][i].value == noPawn)
        buttonDown.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i, j + 2, pos);
          roundP = r;
          hideArrows();
        });
      else buttonDown.hide();
    }

    if (i > 0 && this.tabla[j][i - 1].value != fence) {
      buttonLeft.show();
      buttonLeft.position(getSpaceBetweenRects() / 4 + sp * i, getRectSize() + 1.25 * getSpaceBetweenRects() + sp * j);
      buttonLeft.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      if (this.tabla[j][i - 2].value == pos2 && this.tabla[j][max(0, i - 3)].value != fence && i - 4 >= 0)
        buttonLeft.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i - 4, j, pos);
          roundP = r;
          hideArrows();
        });
      else if (this.tabla[j][i - 2].value == noPawn)
        buttonLeft.mousePressed(function chage() {
          tabla.redrawPawn(i, j, noPawn);
          tabla.redrawPawn(i - 2, j, pos);
          roundP = r;
          hideArrows();
        });
      else buttonLeft.hide();
    }
    if (j > 0 && i - 1 >= 0 && (this.tabla[max(0, j - 1)][i].value != fence && this.tabla[j - 2][i - 1].value != fence && this.tabla[max(0, j - 3)][i].value == fence && this.tabla[j - 2][i].value == pos2) ||
      ((this.tabla[j][max(0, i - 1)].value != fence && this.tabla[max(0, j - 1)][max(0, i - 2)].value != fence && this.tabla[j][max(0, i - 3)].value == fence && this.tabla[j][i - 2].value == pos2))) {
      buttonUpLeft.show();
      buttonUpLeft.position(getSpaceBetweenRects() / 4 + sp * i, getSpaceBetweenRects() / 4 + sp * j);
      buttonUpLeft.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      buttonUpLeft.mousePressed(function chage() {
        tabla.redrawPawn(i, j, noPawn);
        tabla.redrawPawn(i - 2, j - 2, pos);
        roundP = r;
        hideArrows();
      });
    }
    if (j > 0 && i + 2 <= 16 && (this.tabla[max(0, j - 1)][i].value != fence && this.tabla[j - 2][i + 1].value != fence && this.tabla[max(0, j - 3)][i].value == fence && this.tabla[j - 2][i].value == pos2) ||
      (this.tabla[j][i + 1].value != fence && this.tabla[max(0, j - 1)][i + 2].value != fence && this.tabla[j][i + 3].value == fence && this.tabla[j][i + 2].value == pos2)) {
      buttonUpRight.show();
      buttonUpRight.position(2 * getRectSize() + 2.3 * getSpaceBetweenRects() + sp * i, getSpaceBetweenRects() / 4 + sp * j);
      buttonUpRight.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      buttonUpRight.mousePressed(function chage() {
        tabla.redrawPawn(i, j, noPawn);
        tabla.redrawPawn(i + 2, j - 2, pos);
        roundP = r;
        hideArrows();
      });
    }
    if (j < 16 && i + 2 <= 16 && (this.tabla[j + 1][i].value != fence && this.tabla[j + 2][i + 1].value != fence && this.tabla[max(0, j + 3)][i].value == fence && this.tabla[j + 2][i].value == pos2) ||
      ((this.tabla[j][i + 1].value != fence && this.tabla[j + 1][i + 2].value != fence && this.tabla[j][i + 3].value == fence && this.tabla[j][i + 2].value == pos2))) {
      buttonDownRight.show();
      buttonDownRight.position(3.7 * sp + getSpaceBetweenRects() + sp * i, 2.3 * getSpaceBetweenRects() + 2 * getRectSize() + sp * j);
      buttonDownRight.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      buttonDownRight.mousePressed(function chage() {
        tabla.redrawPawn(i, j, noPawn);
        tabla.redrawPawn(i + 2, j + 2, pos);
        roundP = r;
        hideArrows();
      });
    }
    if (j > 0 && i + 2 <= 16 && (this.tabla[j + 1][i].value != fence && this.tabla[j + 2][max(0, i - 1)].value != fence && this.tabla[max(0, j + 3)][i].value == fence && this.tabla[j + 2][i].value == pos2) ||
      (this.tabla[j][max(0, i - 1)].value != fence && this.tabla[j + 1][max(0, i - 2)].value != fence && this.tabla[j][max(0, i - 3)].value == fence && this.tabla[j][max(0, i - 2)].value == pos2)) {
      buttonDownLeft.show();
      buttonDownLeft.position(getSpaceBetweenRects() / 4 + sp * i, 2.25 * getRectSize() + getSpaceBetweenRects() + sp * j);
      buttonDownLeft.size((getRectSize() * 66) / 100, (getRectSize() * 66) / 100);
      buttonDownLeft.mousePressed(function chage() {
        tabla.redrawPawn(i, j, noPawn);
        tabla.redrawPawn(i - 2, j + 2, pos);
        roundP = r;
        hideArrows();
      });
    }
  }
  dragAndDrop() {
    if (!win1 && !win2)
      if (roundP == roundPlayer1 && puttedFencePawn1 <= nrFences && oldClickX >= (getRectSize() - 2 * getTableFromCanvas()) &&
        oldClickX <= (getRectSize() - 2 * getTableFromCanvas() + (getRectSize() + getSpaceBetweenRects()) * 9 + getSpaceBetweenRects()) &&
        oldClickY >= (2 * getTableFromCanvas()) && oldClickY <= getRectSize() - 2 * getTableFromCanvas() + 2 * getTableFromCanvas()) {
        col1 = 'lightblue';
        for (let i = 0; i < 17; i++)
          for (let j = 0; j < 17; j++)
            if (this.tabla[i][j].value == noFence) {
              if (i % 2 == 0 && j % 2 != 0) {
                if (clickX >= (2 * getRectSize() + getTableFromCanvas() + sp * (j - 1)) && clickX <= (2 * getRectSize() + getTableFromCanvas() + getSpaceBetweenRects() + sp * (j - 1)) &&
                  clickY >= (getRectSize() + getTableFromCanvas() + sp * i) && clickY <= (2 * getRectSize() + getTableFromCanvas() + sp * i)) {
                  if (((this.tabla[max(0, i - 2)][j].value == noFence && this.tabla[max(0, i - 1)][j].value == noFence && i >= 2) ||
                      (this.tabla[i + 2][j].value == noFence && this.tabla[i + 1][j].value == noFence) ||
                      (i == 0 && this.tabla[i + 2][j].value == noFence && this.tabla[i + 1][j].value == noFence)) || putted == 1)
                    if ((this.tabla[max(0, i - 1)][j].value == noFence || this.tabla[i + 1][j].value == noFence)) {
                      if (putted == 0) {

                        this.tabla[i][j].value = fence;
                        putted++;
                        fenceX = i;
                        fenceY = j;
                        var speedUp = false;
                        var speedDown = false;

                        if (i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) {
                          this.tabla[i + 1][j].value = fence;
                          this.tabla[i + 2][j].value = fence;
                          putted++;
                          speedUp = true;
                        } else
                        if (i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) {
                          this.tabla[i - 1][j].value = fence;
                          this.tabla[i - 2][j].value = fence;
                          putted++;
                          speedDown = true;
                        }

                        this.hasWayPawn(posPawn1);
                        if (st) {
                          this.tabla[i][j].value = noFence;

                          if ((i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) && speedUp) {
                            this.tabla[i + 1][j].value = noFence;
                            this.tabla[i + 2][j].value = noFence;
                          } else
                          if ((i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) && speedDown) {
                            this.tabla[i - 1][j].value = noFence;
                            this.tabla[i - 2][j].value = noFence;
                          }
                          putted = 0;
                        }

                        this.hasWayPawn(posPawn2);
                        if (st) {
                          this.tabla[i][j].value = noFence;

                          if ((i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) && speedUp) {
                            this.tabla[i + 1][j].value = noFence;
                            this.tabla[i + 2][j].value = noFence;
                          } else
                          if ((i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) && speedDown) {
                            this.tabla[i - 1][j].value = noFence;
                            this.tabla[i - 2][j].value = noFence;
                          }
                          putted = 0;
                        }

                      }
                      if (putted == 1)
                        if (this.tabla[max(0, max(fenceX, i) - 1)][j].value != fence)
                          if ((abs(fenceX - i) == 2 || abs(fenceY - j) == 2) && fenceY == j) {

                            this.tabla[i][j].value = fence;
                            this.tabla[max(fenceX, i) - 1][j].value = fence;
                            putted++;

                            this.hasWayPawn(posPawn1);
                            if (st) {
                              this.tabla[i][j].value = noFence;
                              this.tabla[max(fenceX, i) - 1][j].value = noFence;
                              putted--;
                            }
                            this.hasWayPawn(posPawn2);
                            if (st) {
                              this.tabla[i][j].value = noFence;
                              this.tabla[max(fenceX, i) - 1][j].value = noFence;
                              putted--;
                            }
                          }
                      if (putted == 2) {
                        roundP = roundPlayer2;
                        puttedFencePawn1++;
                        stepSound();
                      }
                    }
                }
              } else if (i % 2 != 0 && j % 2 == 0) {
                if (clickX >= (getRectSize() + getTableFromCanvas() + sp * j) && clickX <= (2 * getRectSize() + getTableFromCanvas() + sp * j) &&
                  clickY >= (1.4 * getRectSize() + getTableFromCanvas() + sp * i) && clickY <= (1.4 * getRectSize() + getTableFromCanvas() + getSpaceBetweenRects() + sp * i)) {
                  if (((this.tabla[i][max(0, j - 2)].value == noFence && this.tabla[i][max(0, j - 1)].value == noFence && j >= 2) ||
                      (this.tabla[i][j + 2].value == noFence && this.tabla[i][j + 1].value == noFence) ||
                      (j == 0 && this.tabla[i][j + 2].value == noFence && this.tabla[i][j + 1].value == noFence)) || putted == 1)
                    if ((this.tabla[i][max(0, j - 1)].value == noFence || this.tabla[i][j + 1].value == noFence)) {
                      if (putted == 0) {
                        this.tabla[i][j].value = fence;
                        putted++;
                        fenceX = i;
                        fenceY = j;
                        var speedRight = false;
                        var speedLeft = false;
                        if (j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) {
                          this.tabla[i][j + 1].value = fence;
                          this.tabla[i][j + 2].value = fence;
                          putted++;
                          speedLeft = true;

                        } else
                        if (j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) {
                          this.tabla[i][j - 1].value = fence;
                          this.tabla[i][j - 2].value = fence;
                          putted++;
                          speedRight = true;
                        }

                        this.hasWayPawn(posPawn1);
                        if (st) {
                          this.tabla[i][j].value = noFence;

                          if ((j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) && speedLeft) {
                            this.tabla[i][j + 1].value = noFence;
                            this.tabla[i][j + 2].value = noFence;
                          } else
                          if ((j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) && speedRight) {
                            this.tabla[i][j - 1].value = noFence;
                            this.tabla[i][j - 2].value = noFence;
                          }
                          putted = 0;
                        }

                        this.hasWayPawn(posPawn2);
                        if (st) {
                          this.tabla[i][j].value = noFence;

                          if ((j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) && speedLeft) {
                            this.tabla[i][j + 1].value = noFence;
                            this.tabla[i][j + 2].value = noFence;
                          } else
                          if ((j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) && speedRight) {
                            this.tabla[i][j - 1].value = noFence;
                            this.tabla[i][j - 2].value = noFence;
                          }
                          putted = 0;
                        }
                      }
                      if (putted == 1)
                        if (this.tabla[i][max(0, max(fenceY, j) - 1)].value != fence)
                          if ((abs(fenceX - i) == 2 || abs(fenceY - j) == 2) && fenceX == i) {
                            this.tabla[i][j].value = fence;
                            this.tabla[i][max(fenceY, j) - 1].value = fence;
                            putted++;

                            this.hasWayPawn(posPawn1);
                            if (st) {
                              this.tabla[i][j].value = noFence;
                              this.tabla[i][max(fenceY, j) - 1].value = noFence;
                              putted--;
                            }
                            this.hasWayPawn(posPawn2);
                            if (st) {
                              this.tabla[i][j].value = noFence;
                              this.tabla[i][max(fenceY, j) - 1].value = noFence;
                              putted--;
                            }
                          }
                      if (putted == 2) {
                        roundP = roundPlayer2;
                        puttedFencePawn1++;
                        stepSound();
                      }
                    }
                }
              }
            }
      } else col1 = 'grey';
    if (roundP == roundPlayer2 && !CPU && puttedFencePawn2 <= nrFences && oldClickX >= (getRectSize() - 2 * getTableFromCanvas()) &&
      oldClickX <= (getRectSize() - 2 * getTableFromCanvas() + (getRectSize() + getSpaceBetweenRects()) * 9 + getSpaceBetweenRects()) &&
      oldClickY >= (getTable() - getRectSize()) && oldClickY <= getTable()) {
      col2 = 'lightblue';
      for (let i = 0; i < 17; i++)
        for (let j = 0; j < 17; j++)
          if (this.tabla[i][j].value == noFence) {
            if (i % 2 == 0 && j % 2 != 0) {
              if (clickX >= (2 * getRectSize() + getTableFromCanvas() + sp * (j - 1)) && clickX <= (2 * getRectSize() + getTableFromCanvas() + getSpaceBetweenRects() + sp * (j - 1)) &&
                clickY >= (getRectSize() + getTableFromCanvas() + sp * i) && clickY <= (2 * getRectSize() + getTableFromCanvas() + sp * i)) {
                if (((this.tabla[max(0, i - 2)][j].value == noFence && this.tabla[max(0, i - 1)][j].value == noFence && i >= 2) ||
                    (this.tabla[i + 2][j].value == noFence && this.tabla[i + 1][j].value == noFence) ||
                    (i == 0 && this.tabla[i + 2][j].value == noFence && this.tabla[i + 1][j].value == noFence)) || putted == 1)
                  if ((this.tabla[max(0, i - 1)][j].value == noFence || this.tabla[i + 1][j].value == noFence)) {
                    if (putted == 0) {

                      this.tabla[i][j].value = fence;
                      putted++;
                      fenceX = i;
                      fenceY = j;
                      var speedUp = false;
                      var speedDown = false;

                      if (i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) {
                        this.tabla[i + 1][j].value = fence;
                        this.tabla[i + 2][j].value = fence;
                        putted++;
                        speedUp = true;
                      } else
                      if (i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) {
                        this.tabla[i - 1][j].value = fence;
                        this.tabla[i - 2][j].value = fence;
                        putted++;
                        speedDown = true;
                      }

                      this.hasWayPawn(posPawn1);
                      if (st) {
                        this.tabla[i][j].value = noFence;

                        if ((i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) && speedUp) {
                          this.tabla[i + 1][j].value = noFence;
                          this.tabla[i + 2][j].value = noFence;
                        } else
                        if ((i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) && speedDown) {
                          this.tabla[i - 1][j].value = noFence;
                          this.tabla[i - 2][j].value = noFence;
                        }
                        putted = 0;
                      }

                      this.hasWayPawn(posPawn2);
                      if (st) {
                        this.tabla[i][j].value = noFence;

                        if ((i == 0 || this.tabla[i - 2][j].value == fence || this.tabla[i - 1][j].value == fence) && speedUp) {
                          this.tabla[i + 1][j].value = noFence;
                          this.tabla[i + 2][j].value = noFence;
                        } else
                        if ((i == 16 || this.tabla[i + 2][j].value == fence || this.tabla[i + 1][j].value == fence) && speedDown) {
                          this.tabla[i - 1][j].value = noFence;
                          this.tabla[i - 2][j].value = noFence;
                        }
                        putted = 0;
                      }

                    }
                    if (putted == 1)
                      if (this.tabla[max(0, max(fenceX, i) - 1)][j].value != fence)
                        if ((abs(fenceX - i) == 2 || abs(fenceY - j) == 2) && fenceY == j) {

                          this.tabla[i][j].value = fence;
                          this.tabla[max(fenceX, i) - 1][j].value = fence;
                          putted++;

                          this.hasWayPawn(posPawn1);
                          if (st) {
                            this.tabla[i][j].value = noFence;
                            this.tabla[max(fenceX, i) - 1][j].value = noFence;
                            putted--;
                          }
                          this.hasWayPawn(posPawn2);
                          if (st) {
                            this.tabla[i][j].value = noFence;
                            this.tabla[max(fenceX, i) - 1][j].value = noFence;
                            putted--;
                          }
                        }
                    if (putted == 2) {
                      roundP = roundPlayer1;
                      puttedFencePawn2++;
                      stepSound();
                    }
                  }
              }
            } else if (i % 2 != 0 && j % 2 == 0) {
              if (clickX >= (getRectSize() + getTableFromCanvas() + sp * j) && clickX <= (2 * getRectSize() + getTableFromCanvas() + sp * j) &&
                clickY >= (1.4 * getRectSize() + getTableFromCanvas() + sp * i) && clickY <= (1.4 * getRectSize() + getTableFromCanvas() + getSpaceBetweenRects() + sp * i)) {
                if (((this.tabla[i][max(0, j - 2)].value == noFence && this.tabla[i][max(0, j - 1)].value == noFence && j >= 2) ||
                    (this.tabla[i][j + 2].value == noFence && this.tabla[i][j + 1].value == noFence) ||
                    (j == 0 && this.tabla[i][j + 2].value == noFence && this.tabla[i][j + 1].value == noFence)) || putted == 1)
                  if ((this.tabla[i][max(0, j - 1)].value == noFence || this.tabla[i][j + 1].value == noFence)) {
                    if (putted == 0) {
                      this.tabla[i][j].value = fence;
                      putted++;
                      fenceX = i;
                      fenceY = j;
                      var speedRight = false;
                      var speedLeft = false;
                      if (j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) {
                        this.tabla[i][j + 1].value = fence;
                        this.tabla[i][j + 2].value = fence;
                        putted++;
                        speedLeft = true;

                      } else
                      if (j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) {
                        this.tabla[i][j - 1].value = fence;
                        this.tabla[i][j - 2].value = fence;
                        putted++;
                        speedRight = true;
                      }

                      this.hasWayPawn(posPawn1);
                      if (st) {
                        this.tabla[i][j].value = noFence;

                        if ((j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) && speedLeft) {
                          this.tabla[i][j + 1].value = noFence;
                          this.tabla[i][j + 2].value = noFence;
                        } else
                        if ((j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) && speedRight) {
                          this.tabla[i][j - 1].value = noFence;
                          this.tabla[i][j - 2].value = noFence;
                        }
                        putted = 0;
                      }

                      this.hasWayPawn(posPawn2);
                      if (st) {
                        this.tabla[i][j].value = noFence;

                        if ((j == 0 || this.tabla[i][j - 2].value == fence || this.tabla[i][j - 1].value == fence) && speedLeft) {
                          this.tabla[i][j + 1].value = noFence;
                          this.tabla[i][j + 2].value = noFence;
                        } else
                        if ((j == 16 || this.tabla[i][j + 2].value == fence || this.tabla[i][j + 1].value == fence) && speedRight) {
                          this.tabla[i][j - 1].value = noFence;
                          this.tabla[i][j - 2].value = noFence;
                        }
                        putted = 0;
                      }
                    }
                    if (putted == 1)
                      if (this.tabla[i][max(0, max(fenceY, j) - 1)].value != fence)
                        if ((abs(fenceX - i) == 2 || abs(fenceY - j) == 2) && fenceX == i) {
                          this.tabla[i][j].value = fence;
                          this.tabla[i][max(fenceY, j) - 1].value = fence;
                          putted++;

                          this.hasWayPawn(posPawn1);
                          if (st) {
                            this.tabla[i][j].value = noFence;
                            this.tabla[i][max(fenceY, j) - 1].value = noFence;
                            putted--;
                          }
                          this.hasWayPawn(posPawn2);
                          if (st) {
                            this.tabla[i][j].value = noFence;
                            this.tabla[i][max(fenceY, j) - 1].value = noFence;
                            putted--;
                          }
                        }
                    if (putted == 2) {
                      roundP = roundPlayer1;
                      puttedFencePawn2++;
                      stepSound();
                    }
                  }
              }
            }
          }
    } else col2 = 'grey';
  }
  hasWayPawn(pos) {

    for (i = 0; i < 17; i++) {
      wayPawn1[i] = [];
      for (j = 0; j < 17; j++) {
        let valueObj = {
          check: false,
          left: false,
          down: false,
          right: false,
          up: false
        }
        wayPawn1[i][j] = valueObj
        if (this.tabla[i][j].value == pos) {
          cui = i;
          cuj = j;
        }
      }
    }
    st = false;
    var checked = 0;
    var positions = 1;
    wayPawn1[cui][cuj].check = true;
    var verify;

    if (pos == posPawn1) verify = 16;
    else verify = 0;

    while (!st && cui != verify) {

      if (cui + 1 <= 16) {
        if (this.tabla[cui + 1][cuj].value == fence || this.tabla[cui + 1][cuj].value == undefined) wayPawn1[cui][cuj].down = true;
      } else wayPawn1[cui][cuj].down = true;

      if (cuj + 1 <= 16) {
        if (this.tabla[cui][cuj + 1].value == fence || this.tabla[cui][cuj + 1].value == undefined) wayPawn1[cui][cuj].right = true;
      } else wayPawn1[cui][cuj].right = true;

      if (cuj - 1 >= 0) {
        if (this.tabla[cui][cuj - 1].value == fence || this.tabla[cui][cuj - 1].value == undefined) wayPawn1[cui][cuj].left = true;
      } else wayPawn1[cui][cuj].left = true;

      if (cui - 1 >= 0) {
        if (this.tabla[cui - 1][cuj].value == fence || this.tabla[cui - 1][cuj].value == undefined) wayPawn1[cui][cuj].up = true;
      } else wayPawn1[cui][cuj].up = true;

      if (this.tabla[cui + 1][cuj].value != fence && (wayPawn1[cui][cuj].down == false) &&
        (!wayPawn1[cui + 2][cuj].down || !wayPawn1[cui + 2][cuj].right || !wayPawn1[cui + 2][cuj].left || !wayPawn1[cui + 2][cuj].up)) {
        cui = cui + 2;
        wayPawn1[cui][cuj].check = true;
        wayPawn1[cui - 2][cuj].down = true;
      } else {
        wayPawn1[cui][cuj].down = true;
        if (this.tabla[cui][cuj + 1].value != fence && (wayPawn1[cui][cuj].right == false) &&
          (!wayPawn1[cui][cuj + 2].down || !wayPawn1[cui][cuj + 2].right || !wayPawn1[cui][cuj + 2].left || !wayPawn1[cui][cuj + 2].up)) {
          cuj = cuj + 2;
          wayPawn1[cui][cuj].check = true;
          wayPawn1[cui][cuj - 2].right = true;
        } else {
          wayPawn1[cui][cuj].right = true;
          if ((this.tabla[cui][max(0, cuj - 1)].value != fence) && (wayPawn1[cui][cuj].left == false) &&
            (!wayPawn1[cui][cuj - 2].down || !wayPawn1[cui][cuj - 2].right || !wayPawn1[cui][cuj - 2].left || !wayPawn1[cui][cuj - 2].up)) {
            cuj = cuj - 2;
            wayPawn1[cui][cuj].check = true;
            wayPawn1[cui][cuj + 2].left = true;
          } else {
            wayPawn1[cui][cuj].left = true;
            if (cui - 2 >= 0)
              if (this.tabla[max(0, cui - 1)][cuj].value != fence && (wayPawn1[cui][cuj].up == false) &&
                (!wayPawn1[cui - 2][cuj].down || !wayPawn1[cui - 2][cuj].right || !wayPawn1[cui - 2][cuj].left || !wayPawn1[cui - 2][cuj].up)) {
                cui = cui - 2;
                wayPawn1[cui][cuj].check = true;
                wayPawn1[cui + 2][cuj].up = true;
              } else {
                wayPawn1[cui][cuj].up = true;
              }
          }
        }
      }

      positions = 0;
      checked = 0;
      for (i = 0; i < 17; i++)
        for (j = 0; j < 17; j++) {
          if (wayPawn1[i][j].check == true) positions++;
          if (wayPawn1[i][j].left && wayPawn1[i][j].up && wayPawn1[i][j].down && wayPawn1[i][j].right) checked++;
        }

      if (wayPawn1[cui][cuj].left && wayPawn1[cui][cuj].up && wayPawn1[cui][cuj].down && wayPawn1[cui][cuj].right) {
        st = true;
      }
      if (st) {
        for (i = 0; i < 17; i++)
          for (j = 0; j < 17; j++) {
            if (wayPawn1[i][j].check && (!wayPawn1[i][j].left || !wayPawn1[i][j].up || !wayPawn1[i][j].right || !wayPawn1[i][j].down)) {
              cui = i;
              cuj = j;
              st = false;
            }
          }

      }
    }
  }
  cpuGame() {
    if (roundP == roundPlayer2 && CPU && !medium && !win1) {
      for (i = 0; i < 17; i++)
        for (j = 0; j < 17; j++) {
          if (this.tabla[i][j].value == posPawn2) {
            cui = i;
            cuj = j;
          }
        }

      var action;
      var directions = 0;
      var direction = 0;
      var up = false;
      var down = false;
      var right = false;
      var left = false;
      action = floor(random(5));
      //0,2,3 = moving pawn   1 = putting fences
      if (puttedFencePawn2 == 10 && action == 1) action = 2;

      if (action != 1) {
        if (this.tabla[cui - 1][cuj].value == noFence)
          if (this.tabla[cui - 2][cuj].value != posPawn1) {
            directions++;
            up = true;
          }
        if (this.tabla[cui + 1][cuj].value == noFence)
          if (this.tabla[cui + 2][cuj].value != posPawn1) {
            directions++;
            down = true;
          }
        if (this.tabla[cui][cuj - 1].value == noFence)
          if (this.tabla[cui][cuj - 2].value != posPawn1) {
            directions++;
            left = true;
          }
        if (this.tabla[cui][cuj + 1].value == noFence)
          if (this.tabla[cui][cuj + 2].value != posPawn1) {
            directions++;
            right = true;
          }

        direction = floor(random(directions));

        if (direction == 0 && up) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui - 2][cuj].value = posPawn2;
        } else if (direction == 0 && right) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj + 2].value = posPawn2;
        } else if (direction == 0 && left) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj - 2].value = posPawn2;
        } else if (direction == 0 && down) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui + 2][cuj].value = posPawn2;
        }

        if (direction == 1 && right) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj + 2].value = posPawn2;
        } else if (direction == 1 && left) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj - 2].value = posPawn2;
        } else if (direction == 1 && up) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui - 2][cuj].value = posPawn2;
        } else if (direction == 1 && down) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui + 2][cuj].value = posPawn2;
        }

        if (direction == 2 && up) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui - 2][cuj].value = posPawn2;
        } else if (direction == 2 && left) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj - 2].value = posPawn2;
        } else if (direction == 2 && right) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj + 2].value = posPawn2;
        } else if (direction == 2 && down) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui + 2][cuj].value = posPawn2;
        }

        if (direction == 3 && down) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui + 2][cuj].value = posPawn2;
        } else if (direction == 3 && up) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui - 2][cuj].value = posPawn2;
        } else if (direction == 3 && left) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj - 2].value = posPawn2;
        } else if (direction == 3 && right) {
          this.tabla[cui][cuj].value = noPawn;
          this.tabla[cui][cuj + 2].value = posPawn2;
        }

        roundP = roundPlayer1;
        if (directions == 0) action = 1;
      }
      if (action == 1) {
        var direction = floor(random(2));
        var pozitionX, pozitionY;
        //1 = horizontal   0 = vertical
        if (direction == 1) {

          do {
            pozitionX = floor(random(17));
            if (pozitionX % 2 == 0) pozitionX++;
            if (pozitionX == 17) pozitionX = 15;
            pozitionY = floor(random(17));
            if (pozitionY % 2 == 0) pozitionY++;
            if (pozitionY == 17) pozitionY = 15;
          }
          while (this.tabla[pozitionX][pozitionY].value != noFence ||
            this.tabla[pozitionX][pozitionY - 1].value != noFence ||
            this.tabla[pozitionX][pozitionY + 1].value != noFence)

          this.tabla[pozitionX][pozitionY].value = fence;
          this.tabla[pozitionX][pozitionY - 1].value = fence;
          this.tabla[pozitionX][pozitionY + 1].value = fence;
          puttedFencePawn2++;
          roundP = roundPlayer1;

          this.hasWayPawn(posPawn1)
          if (st) {
            this.tabla[pozitionX][pozitionY].value = noFence
            this.tabla[pozitionX][pozitionY - 1].value = noFence;
            this.tabla[pozitionX][pozitionY + 1].value = noFence;
            puttedFencePawn2--;
            roundP = roundPlayer2;
          }
          this.hasWayPawn(posPawn2)
          if (st) {
            this.tabla[pozitionX][pozitionY].value = noFence
            this.tabla[pozitionX][pozitionY - 1].value = noFence;
            this.tabla[pozitionX][pozitionY + 1].value = noFence;
            puttedFencePawn2--;
            roundP = roundPlayer2;
          }
        }

        if (direction == 0) {

          pozitionX = floor(random(17));
          do {
            if (pozitionX % 2 == 0) pozitionX++;
            if (pozitionX == 17) pozitionX = 15;
            pozitionY = floor(random(17));
            if (pozitionY % 2 == 0) pozitionY++;
            if (pozitionY == 17) pozitionY = 15;
          }
          while (this.tabla[pozitionX][pozitionY].value != noFence ||
            this.tabla[pozitionX - 1][pozitionY].value != noFence ||
            this.tabla[pozitionX + 1][pozitionY].value != noFence);


          this.tabla[pozitionX][pozitionY].value = fence;
          this.tabla[pozitionX - 1][pozitionY].value = fence;
          this.tabla[pozitionX + 1][pozitionY].value = fence;
          puttedFencePawn2++;
          roundP = roundPlayer1;

          this.hasWayPawn(posPawn1)
          if (st) {
            this.tabla[pozitionX][pozitionY].value = noFence
            this.tabla[pozitionX - 1][pozitionY].value = noFence;
            this.tabla[pozitionX + 1][pozitionY].value = noFence;
            puttedFencePawn2--;
            roundP = roundPlayer2;
          }
          this.hasWayPawn(posPawn2)
          if (st) {
            this.tabla[pozitionX][pozitionY].value = noFence
            this.tabla[pozitionX - 1][pozitionY].value = noFence;
            this.tabla[pozitionX + 1][pozitionY].value = noFence;
            puttedFencePawn2--;
            roundP = roundPlayer2;
          }
        }
      }
    }
      if(roundP==roundPlayer2 && CPU && medium && !win1){
        var blocked = false;
        var freeSpaceRight;
        var freeSpaceLeft;
        for (i = 0; i < 17; i++)
        for (j = 0; j < 17; j++) {
          if (this.tabla[i][j].value == posPawn2) {
            cui = i;
            cuj = j;
          }
          if(this.tabla[i][j].value == posPawn1){
            pli = i;
            plj = j;
          }
        }
          if(pli>=10&&cui>=8 || (pli>=10 && 16-pli < cui))
            {
              for(i=15;i>pli;i=i-2){
              if(this.tabla[i][plj].value == fence){
                 blocked = true;
              }
            }
          if(!blocked && roundP == roundPlayer2 && puttedFencePawn2<=nrFences)
          for(i=pli+1;i<=15;i=i+2){ 
            if(this.tabla[i][max(0,plj-1)].value != fence && this.tabla[i][max(0,plj-2)].value != fence && roundP == roundPlayer2){
              this.tabla[i][plj].value = fence;
              this.tabla[i][max(0,plj-1)].value = fence;
              this.tabla[i][max(0,plj-2)].value = fence;
              puttedFencePawn2++;
              roundP = roundPlayer1;

              this.hasWayPawn(posPawn1)
              if (st) {
                this.tabla[i][plj].value = noFence
                this.tabla[i][max(0,plj - 1)].value = noFence;
                this.tabla[i][max(0,plj - 2)].value = noFence;
                puttedFencePawn2--;
                roundP = roundPlayer2;
              }
              this.hasWayPawn(posPawn2)
              if (st) {
                this.tabla[i][plj].value = noFence
                this.tabla[i][max(0,plj - 2)].value = noFence;
                this.tabla[i][max(0,plj - 1)].value = noFence;
                puttedFencePawn2--;
                roundP = roundPlayer2;
              }           
            }
            else if(plj+2<=16 && this.tabla[i][plj+1].value != fence && this.tabla[i][plj+2].value != fence && roundP == roundPlayer2){
              this.tabla[i][plj].value = fence;
              this.tabla[i][plj+1].value = fence;
              this.tabla[i][plj+2].value = fence;
              puttedFencePawn2++;
              roundP = roundPlayer1;

              this.hasWayPawn(posPawn1)
              if (st) {
                this.tabla[i][plj].value = noFence
                this.tabla[i][plj + 1].value = noFence;
                this.tabla[i][plj + 2].value = noFence;
                puttedFencePawn2--;
                roundP = roundPlayer2;
              }
              this.hasWayPawn(posPawn2)
              if (st) {
                this.tabla[i][plj].value = noFence
                this.tabla[i][plj + 2].value = noFence;
                this.tabla[i][plj + 1].value = noFence;
                puttedFencePawn2--;
                roundP = roundPlayer2;
              }           
            }
      for (i = 0; i < 17; i++)
        for (j = 0; j < 17; j++) 
          if (this.tabla[i][j].value == posPawn2) {
            cui = i;
            cuj = j;
          }
          }
        } blocked = false;
          for(i=1;i<cui;i+=2)
            if(this.tabla[i][cuj].value == fence  && roundP == roundPlayer2){
              blocked = true; 
                for(j=cuj;j<17;j+=2) if(this.tabla[i][j].value != fence) {freeSpaceRight = j; console.log(j); j=17;}
                for(j=cuj;j>=0;j-=2) if(this.tabla[i][j].value != fence) {freeSpaceLeft = j; console.log(j); j=-1;}
                if(freeSpaceLeft==undefined) freeSpaceLeft=-16;
                console.log(freeSpaceLeft, freeSpaceRight);
            }
            if(blocked && roundP == roundPlayer2){
              if(this.tabla[cui][cuj+1].value == noFence && freeSpaceRight-cuj<= cuj-freeSpaceLeft){
                if(this.tabla[cui][cuj+2].value == posPawn1 && this.tabla[cui][cuj+3].value != fence && cuj+4 <=16)
                {this.tabla[cui][cuj+2].value = posPawn2;
                this.tabla[cui][cuj].value = noPawn;
                roundP = roundPlayer1; lastY = cui; lastX = cuj;
              }  else if(this.tabla[cui][cuj+2].value == posPawn1 && this.tabla[cui][cuj+3].value == fence && this.tabla[cui-1][cuj+2].value != fence && cuj+4<=16){
                  this.tabla[cui-2][cui+2].value = posPawn2;
                  this.tabla[cui][cuj].value = noPawn;
                  roundP = roundPlayer1; lastY = cui; lastX = cuj;
                }
               else { 
                  this.tabla[cui][cuj+2].value = posPawn2;
                  this.tabla[cui][cuj].value = noPawn;
                  roundP = roundPlayer1; lastY = cui; lastX = cuj;
              }}
              if(this.tabla[cui][cuj-1].value == noFence && roundP == roundPlayer2 && freeSpaceLeft!=-16){
                if(this.tabla[cui][max(0,cuj-2)].value == posPawn1 && this.tabla[cui][cuj-3].value != fence && cuj-4 >=0)
                {this.tabla[cui][cuj-2].value = posPawn2;
                this.tabla[cui][cuj].value = noPawn;
                roundP = roundPlayer1; lastY = cui; lastX = cuj;
              }  else if(this.tabla[cui][cuj-2].value == posPawn1 && this.tabla[cui][cuj-3].value == fence && this.tabla[cui-1][cuj-2].value != fence && cuj-4>=0){
                  this.tabla[cui-2][cui+2].value = posPawn2;
                  this.tabla[cui][cuj].value = noPawn;
                  roundP = roundPlayer1; lastY = cui; lastX = cuj;
            }  else {
                 this.tabla[cui][cuj-2].value = posPawn2;
                 this.tabla[cui][cuj].value = noPawn;
                 roundP = roundPlayer1; lastY = cui; lastX = cuj;
            }}
            
            if((this.tabla[cui][cuj+1].value == fence && this.tabla[cui][cuj-1].value == fence && roundP == roundPlayer2) ||
              (roundP == roundPlayer2 && this.tabla[cui][cui+1].value == fence && freeSpaceLeft ==-16) ||
              (roundP == roundPlayer2 && this.tabla[cui][cuj-1].value == fence && freeSpaceRight == undefined)){
              this.tabla[cui][cuj].value = noPawn;
              this.tabla[cui+2][cuj].value = posPawn2;
              roundP = roundPlayer1; lastY = cui; lastX = cuj;
            }
          }

          if(this.tabla[max(0,cui-1)][cuj].value != fence && roundP == roundPlayer2)
            {if(cui-2==pli&&cuj==plj && this.tabla[cui-3][cuj].value!=fence && cui-4>=0)
              {this.tabla[cui][cuj].value = noPawn;
               this.tabla[cui-4][cuj].value = posPawn2;
               roundP = roundPlayer1;
               lastY = cui; lastX = cuj;
              } else if(roundP == roundPlayer2){
               this.tabla[cui][cuj].value = noPawn;
               this.tabla[max(0,cui-2)][cuj].value = posPawn2;
               roundP = roundPlayer1;
               lastY = cui; lastX = cuj;
              }
            }
      }
  }
}

var tabla = new Tabla();


function drawPawns() {
  tabla.drawPawns();
  if (win1) {
    winner(player1);
  }
  if (win2) {
    winner(player2);
  }
}

function init() {
  tabla.init();
}

function clicked() {
  if (putted != 1) {
    oldClickX = mouseX;
    oldClickY = mouseY;
  }
  clickX = 0;
  clickY = 0;
  if (putted == 2) {
    putted = 0;
    fenceX = -1;
    fenceY = -1;
  }
  tabla.clicked();
  tabla.dragAndDrop();
  tabla.cpuGame();
 // console.clear();
}

function mouseDragged() {
  clickX = mouseX;
  clickY = mouseY;
  tabla.dragAndDrop();
}


function createButtons() {
  buttonRight = createButton('→');
  buttonLeft = createButton('←');
  buttonUp = createButton('↑');
  buttonDown = createButton('↓');
  buttonUpLeft = createButton('↖');
  buttonUpRight = createButton('↗');
  buttonDownLeft = createButton('↙');
  buttonDownRight = createButton('↘');
  hideArrows();
}

function hideArrows() {
  setSelect(false);
  buttonRight.hide();
  buttonLeft.hide();
  buttonUp.hide();
  buttonDown.hide();
  buttonUpLeft.hide();
  buttonUpRight.hide();
  buttonDownLeft.hide();
  buttonDownRight.hide();
}