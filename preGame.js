var player1 = new Object();
var player2 = new Object();
var inp1, inp2, button1, button2, button3, radio;
let col, value = 0,
    back = true;

function setPreGame() {
    inp2 = createInput("player 2");
    inp2.position(windowWidth / 1.6, windowHeight / 3);
    button2 = createButton("Set name for player 2");
    button2.position(windowWidth / 1.6, windowHeight / 2.5);
    button2.mousePressed(name2);
    button2.style('background-color', '#85241d');
    inp1 = createInput("player 1");
    inp1.position(windowWidth / 3.8, windowHeight / 3);
    button1 = createButton("Set name for player 1");
    button1.position(windowWidth / 3.8, windowHeight / 2.5);
    button1.mousePressed(name1);
    button1.style('background-color', '#85241d');
    button3 = createButton("START");
    button3.position(windowWidth / 1.45, windowHeight / 1.5);
    button3.mousePressed(starting);
    button3.size(windowWidth / 20, windowHeight / 20);
    button3.style('background-color', '#696969');
    radio = createRadio();
    radio.option('Player vs Player');
    radio.option('Player vs CPU (easy)');
    radio.option('Player vs CPU (medium)')
    radio.style('width', '290px');
    radio.position(windowWidth / 3.8, windowHeight / 1.75);
}

function starting() {
    radio.hide();
    inp2.hide();
    button2.hide();
    inp1.hide();
    button1.hide();
    button3.hide();
    noCanvas();

    player2.name = inp2.value();
    player1.name = inp1.value();

    startTrue(radio.value());
}


function name2() {
    inp2.hide();
    button2.hide();
    player2['name'] = inp2.value();
}

function name1() {
    inp1.hide();
    button1.hide();
    player1['name'] = inp1.value();
}

function colorChangeOnInput() {

    fill("#696969");
    rect(windowWidth / 100, windowHeight / 50, windowWidth / 2.08, 150);


    if (value <= 255 && back)
        value++;
    else back = false;
    if (value >= 0 && !back)
        value--;
    else back = true;
    col = color(value);
    inp2.style('background-color', col);
    inp1.style('background-color', col);

}