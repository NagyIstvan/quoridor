var table;

function getTable() {
  return table;
}
var tableForFences;

function getTableForFences() {
  return tableForFences;
}
var rectSize;

function getRectSize() {
  return rectSize;
}
var spaceBetweenRects;

function getSpaceBetweenRects() {
  return spaceBetweenRects;
}
var tableFromCanvas;

function getTableFromCanvas() {
  return tableFromCanvas;
}
var backg;
var circkleSize;

function getCirckleSize() {
  return circkleSize;
}

var select = false;
const nrFences = 9;
var start = false;
var restart;
var CPU = false;
var gratulation;
var gratulated;
var stepsound;
var slider;
var soundIconX;
var soundIconY;
var medium = false;

function setup() {

  gratulated = false;
  start = false;
  setPreGame();

  if (!start) {
    var cnv = createCanvas(windowWidth / 2, windowHeight / 2);
    cnv.position(windowWidth / 4, windowHeight / 4);
  }

  init();
  if ((windowWidth * 80) / 100 < windowHeight)
    table = (windowWidth * 80) / 100;
  else table = windowHeight

  spaceBetweenRects = (table * 1.6) / 100;
  tableFromCanvas = (table * 0.5) / 100;
  rectSize = (table - 8 * spaceBetweenRects - 2 * tableFromCanvas) / 11;
  circkleSize = table * 2.7 / 100;
  soundIconX = windowWidth - 3.2 * rectSize;
  soundIconY = windowHeight - 2.17 * rectSize;
  createButtons();

  slider = createSlider(0, 1, 0.5, 0.01);
  slider.position(windowWidth - 2.5 * rectSize, windowHeight - 2.5 * rectSize);
  slider.hide();
  restart = createButton("RESTART");
  restart.position(windowWidth - 2 * rectSize, windowHeight - 1.5 * rectSize);
  restart.mousePressed(restartGame);
  restart.size(rectSize + rectSize / 2, rectSize);
  restart.style('background-color', '#696969');
  restart.hide();
}

function preload() {
  backg = loadImage('pictures/background.jpg');
  gratulation = loadSound("sounds/grat2.aac");
  stepsound = loadSound("sounds/step.mp3");

}

function stepSound() {
  stepsound.play();
}

function restartGame() {
  gratulation.stop();
  restart.hide();
  slider.hide();
  hideArrows();
  setup();
}

function startTrue(gametype) {
  start = true;
  var cnv2 = createCanvas(windowWidth, windowHeight);
  cnv2.position(0, 0);
  restart.show();
  slider.show();
  if (gametype == 'Player vs CPU (easy)') CPU = true;
  else CPU = false;
  if (gametype == 'Player vs CPU (medium)') {medium = true; CPU= true;}
  else medium = false;
}


function winner(player) {
  strokeWeight(5);
  stroke("#99a3a2");
  frameRate(5);
  fill(floor(random(255)), floor(random(255)), floor(random(255)));
  textStyle(BOLDITALIC)
  textSize(windowWidth / 15);
  if (windowWidth / 15 > 50) strokeWeight(10);
  if (!player.name) player.name = 'Somebody';
  if (windowHeight <= 2 * table)
    text(player.name + " is the winner! 🏆👏🏆", spaceBetweenRects, windowHeight / 2);
  else
    text(player.name + " is the winner! 🏆👏🏆", spaceBetweenRects, 1.5 * table);
  strokeWeight(1);
  if ((win1 || win2) && !gratulated) {
    gratulation.play();
    gratulated = true;
  }
}

function setSelect(select) {
  this.select = select
}

function draw() {
  gratulation.setVolume(slider.value());
  stepsound.setVolume(slider.value());
  if (start) {
    background(backg);
    textStyle(NORMAL);
    textSize(30);
    text("🔊", soundIconX, soundIconY);
    strokeWeight(5);
    stroke("black");
    fill('#85241d');
    textStyle(BOLDITALIC)
    if (windowHeight <= 2 * table) {
      text(player1.name, table + 30, 2 * rectSize);
      fill('yellow');
      circle(table + 14, 2 * rectSize - 7, 18);
      fill('#85241d');
      if (CPU)
        text(player2.name + " (CPU)", table + 30, table - rectSize * 2);
      else
        text(player2.name, table + 30, table - rectSize * 2);
      fill('red');
      circle(table + 14, table - rectSize * 2 - 7, 18);
      if (roundP == 1 && !win1 && !win2) {
        fill('#d6d938');
        text(player1.name + "'s round", table + 10, windowHeight / 2);
      }
      if (roundP == 2 && !win1 && !win2) {
        fill('#c90000');
        text(player2.name + "'s round", table + 10, windowHeight / 2);
      }
      strokeWeight(1);
    } else {
      text(player1.name, rectSize * 2, table + 2 * rectSize);
      fill('yellow');
      circle(rectSize, table + 2 * rectSize - 7, 18);
      fill('#85241d');
      if (CPU)
        text(player2.name + " (CPU)", rectSize * 2, 2 * table - rectSize * 2);
      else
        text(player2.name, rectSize * 2, 2 * table - rectSize * 2);
      fill('red');
      circle(rectSize, 2 * table - rectSize * 2 - 7, 18);
      if (roundP == 1 && !win1 && !win2) {
        fill('#d6d938');
        text(player1.name + "'s round", 2 * rectSize, 1.5 * table);
      }
      if (roundP == 2 && !win1 && !win2) {
        fill('#c90000');
        text(player2.name + "'s round", 2 * rectSize, 1.5 * table);
      }
      strokeWeight(1);
    }
    if (!select) hideArrows();
    drawTable();
    drawFences(nrFences - puttedFencePawn1, 2 * tableFromCanvas, col1);
    drawFences(nrFences - puttedFencePawn2, table - rectSize, col2);
    drawPawns();
  } else {
    frameRate(60);
    background("grey");
    colorChangeOnInput();
  }
}

function mouseClicked() {
  if (start)
    clicked();
}