function drawTable() {
    fill('#85241d');
    rect(0, 0, getTable(), getTable());
    fill('black');
    quad(getTableFromCanvas(), getTableFromCanvas(), getRectSize(), getRectSize(), getTable() - getRectSize(), getRectSize(), getTable() - getTableFromCanvas(), getTableFromCanvas());
    quad(getTableFromCanvas(), getTable() - getTableFromCanvas(), getTable() - getTableFromCanvas(), getTable() - getTableFromCanvas(), getTable() - getRectSize(), getTable() - getRectSize(), getRectSize(), getTable() - getRectSize());
    fill('black');
    for (i = 0; i < 9; i++)
        for (j = 0; j < 9; j++)
            rect(getRectSize() + getTableFromCanvas() + (getRectSize() + getSpaceBetweenRects()) * i, getRectSize() + getTableFromCanvas() + (getRectSize() + getSpaceBetweenRects()) * j, getRectSize(), getRectSize());
}

function drawFences(fences, y, col) {
    for (let i = 0; i <= fences; i++) {
        fill(col);
        strokeWeight(2);
        stroke("#292726");
        rect(getRectSize() - 2 * getTableFromCanvas() + (getRectSize() + getSpaceBetweenRects()) * i, y, getSpaceBetweenRects(), getRectSize() - 2 * getTableFromCanvas())
        strokeWeight(1);
        stroke("black");
    }
}